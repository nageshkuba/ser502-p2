 @echo off
:CHECK_HOME
 if "%CANYON_HOME%" == "" goto FINISH
:LAUNCH_RUNTIME
 java -jar "%CANYON_HOME%/src/Runtime/Canyon/lib/canyon.jar" %1
 goto FINISH
:NO_ENV_SET
 echo Unable to find the environment variable CANYON_HOME 
 goto FINISH
:FINISH
