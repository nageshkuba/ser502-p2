package asu.msse.canyon.main;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;


public class ExecutionManager {
	public  Map<String,Term> map;
	public static  Map<String,Term> globalmap=new HashMap<String,Term> ();
	public static int scope=-1;
	public static Stack <Term> stack=new Stack<Term>();
	public List<String> list;
	private boolean done;
	private int offset;
	public ExecutionManager(int offset){
		list=new ArrayList<String>();
		map=new HashMap<String,Term> ();
		this.offset=offset;
	}
	
	public void execute() {
		stack.push(new Term("str","returnFunct"));
		scope++;
		int i=0;
		for(i=0;i<list.size() && !done;i++){
			String inst=list.get(i);
			String[] arr=inst.split(" ");
			String s="";
			String op="";
			String instruction=arr[0].trim();
			boolean b=false;
			if(arr.length>1){
				op=arr[arr.length-1];
			}
			Term t,n=null,n1=null,n2=null;
			
			switch(instruction){
			case "saveint":
				t=stack.pop();
				t.setType("int");
				saveVaiable(op,t);break;
			case "savedbl":
				t=stack.pop();
				t.setType("double");
				saveVaiable(op,t);break;
			case "savestr":
				t=stack.pop();
				t.setType("str");
				saveVaiable(op,t);break;
			case "read":
				Scanner in = new Scanner(System.in);
				s = in.next();
				n=getVaiable(op);
				n.setValue(s);  
				break;
			case "pushint":
				n=new Term("int",op);
				stack.push(n);break;
			case "pushdbl":
				n=new Term("double",op);
				stack.push(n);break;
			case "pushstr":
				n=new Term("str",op);
				stack.push(n);break;
			case "not":
				n=stack.pop();
				if(n.getType()!="str" ){
					b = Integer.parseInt(n.getValue()) >0? false : true;
				}
				if (b) {
					i++;
				}break;
			case "neq":
				n2 = stack.pop();
				n1 = stack.pop();
				if (n1.getType() == "str" || n2.getType() == "str") {
					b = n1.getValue().equals(n2.getValue()) ? false : true;
				} else if (n1.getType() == "int" || n2.getType() == "int") {

					b = Integer.parseInt(n1.getValue()) != Integer.parseInt(n2
							.getValue()) ? true : false;
				}else if (n1.getType() == "double" || n2.getType() == "double") {

					b = Double.parseDouble(n1.getValue()) !=Double.parseDouble(n2
							.getValue()) ? true : false;
				}
				if (b) {
					i++;
				}break;
			case "lte":
				n2 = stack.pop();
				n1 = stack.pop();
				if (n1.getType() == "str" || n2.getType() == "str") {
					System.out.println(" Operation not supported!!");
				} else if (n1.getType() == "int" || n2.getType() == "int") {

					b = Integer.parseInt(n1.getValue()) <= Integer.parseInt(n2
							.getValue()) ? true : false;
				}else if (n1.getType() == "double" || n2.getType() == "double") {

					b = Double.parseDouble(n1.getValue()) <=Double.parseDouble(n2
							.getValue()) ? true : false;
				}
				if (b) {
					i++;
				}break;
			case "and":
				n2 = stack.pop();
				n1 = stack.pop();
				if (n1.getType() == "str" || n2.getType() == "str") {
					System.out.println(" Operation not supported!!");
				}  else if (n1.getType() == "int" || n2.getType() == "int") {
					b=Integer.parseInt(n1.getValue())>0 &&  Integer.parseInt(n2
							.getValue())>0 ? true : false;
				}else if (n1.getType() == "double" || n2.getType() == "double") {
					b=Double.parseDouble(n1.getValue())>0 &&  Double.parseDouble(n2
							.getValue())>0 ? true : false;
				} 
				if (b) {
					i++;
				}break;
			case "or":
				n2 = stack.pop();
				n1 = stack.pop();
				if (n1.getType() == "str" || n2.getType() == "str") {
					System.out.println(" Operation not supported!!");
				} else if (n1.getType() == "int" || n2.getType() == "int") {
					b=Integer.parseInt(n1.getValue())>0 &&  Integer.parseInt(n2
							.getValue())>0 ? true : false;
				}else if (n1.getType() == "double" || n2.getType() == "double") {
					b=Double.parseDouble(n1.getValue())>0 &&  Double.parseDouble(n2
							.getValue())>0 ? true : false;
				} 
				if (b) {
					i++;
				}break;
			case "gt":
				n2 = stack.pop();
				n1 = stack.pop();
				if (n1.getType() == "str" || n2.getType() == "str") {
					System.out.println(" Operation not supported!!");
				} else {
					b = Integer.parseInt(n1.getValue()) > Integer.parseInt(n2
							.getValue()) ? true : false;
				}
				if (b) {
					i++;
				}break;
			case "lt":
				n2 = stack.pop();
				n1 = stack.pop();
				if (n1.getType() == "str" || n2.getType() == "str") {
					System.out.println(" Operation not supported!!");
				} else {
					
					
					
					b = Integer.parseInt(n1.getValue()) < Integer.parseInt(n2
							.getValue()) ? true : false;
				}
				if (b) {
					i++;
				}break;
			case "gte":
				n1 = stack.pop();
				n2 = stack.pop();
				if (n1.getType() == "str" || n2.getType() == "str") {
					System.out.println(" Operation not supported!!");
				} else {
					b = Integer.parseInt(n1.getValue()) >= Integer.parseInt(n2
							.getValue()) ? true : false;
				}
				if (b) {
					i++;
				}break;
			case "pushiden":n=getVaiable(op);
							stack.push(n);break;
			case "mul":n1=stack.pop();
					n2=stack.pop();
					if(n1.getType()=="int" && n2.getType()=="int"){
						int _n1=Integer.parseInt(n1.getValue());
						int _n2=Integer.parseInt(n2.getValue());
						int res=_n1*_n2;
						stack.push(new Term("int",res+""));
					}else if(n1.getType()=="double" && n2.getType()=="double"){
						double _n1=Double.parseDouble(n1.getValue());
						double _n2=Double.parseDouble(n2.getValue());
						double res=_n1*_n2;
						stack.push(new Term("double",res+""));
					}break;
			case "add":n1=stack.pop();
			n2=stack.pop();
			if(n1.getType()=="int" && n2.getType()=="int"){
				System.out.println("n1.getValue()"+n1.getValue());
				int _n1=Integer.parseInt(n1.getValue());
				int _n2=Integer.parseInt(n2.getValue());
				int res=_n1+_n2;
				stack.push(new Term("int",res+""));
			}else if(n1.getType()=="double" && n2.getType()=="double"){
				double _n1=Double.parseDouble(n1.getValue());
				double _n2=Double.parseDouble(n2.getValue());
				double res=_n1+_n2;
				stack.push(new Term("double",res+""));
			}break;
			case "sub":n1=stack.pop();
			n2=stack.pop();
			if(n1.getType()=="int" || n2.getType()=="int"){
				int _n1=Integer.parseInt(n1.getValue());
				int _n2=Integer.parseInt(n2.getValue());
				int res=_n1-_n2;
				stack.push(new Term("int",res+""));
			}else if(n1.getType()=="double" && n2.getType()=="double"){
				double _n1=Double.parseDouble(n1.getValue());
				double _n2=Double.parseDouble(n2.getValue());
				double res=_n1-_n2;
				stack.push(new Term("double",res+""));
			}break;
			case "div":n1=stack.pop();
			n2=stack.pop();
			if(n1.getType()=="int" || n2.getType()=="int"){
				int _n1=Integer.parseInt(n1.getValue());
				int _n2=Integer.parseInt(n2.getValue());
				int res=_n1/_n2;
				stack.push(new Term("int",res+""));
			}else if(n1.getType()=="double" && n2.getType()=="double"){
				double _n1=Double.parseDouble(n1.getValue());
				double _n2=Double.parseDouble(n2.getValue());
				double res=_n1/_n2;
				stack.push(new Term("double",res+""));
			}break;
			case "rem":n1=stack.pop();
			n2=stack.pop();
			if(n1.getType()=="int" || n2.getType()=="int"){
				int _n1=Integer.parseInt(n1.getValue());
				int _n2=Integer.parseInt(n2.getValue());
				int res=_n1%_n2;
				stack.push(new Term("int",res+""));
			}else if(n1.getType()=="double" && n2.getType()=="double"){
				double _n1=Double.parseDouble(n1.getValue());
				double _n2=Double.parseDouble(n2.getValue());
				double res=_n1%_n2;
				stack.push(new Term("double",res+""));
			}break;
			case "disp":
			   System.out.println(stack.pop().getValue() );break;
			case "inc++":n1=stack.pop();
							n1.setValue(Integer.parseInt(n1.getValue())+1+"");
							stack.push(n1);
			 
				break;
			case "dec--":n1=stack.pop();
			n1.setValue(Integer.parseInt(n1.getValue())-1+"");
			stack.push(n1);

			break;
			case "assn":
				n=stack.pop();
				saveVaiable(op,n); break;
			case "goto":i=Integer.parseInt(op)-1-offset;break;	
			case "eq":	n1=stack.pop();
						n2=stack.pop();
						if(n1.getType()=="str"||n2.getType()=="str"){
						b=n1.getValue().equals(n2.getValue())?true:false;
						}else if(n1.getType()=="int"||n2.getType()=="int"){
							b=Integer.parseInt(n1.getValue())==Integer.parseInt(n2.getValue())?true:false;
						}else if(n1.getType()=="double"||n2.getType()=="double"){
							b=Double.parseDouble(n1.getValue())==Double.parseDouble(n2.getValue())?true:false;
						}
						if(b){
							i++;
						}break;
			case "funcall":
					stack.push(new Term("str",op));
					break;
			case "call":
					String funName=stack.pop().getValue();
					MyFunction  fun=Canyon.func.get(funName);
					ExecutionManager can=new ExecutionManager(fun.startIndex);
					can.list=fun.instr;
					
					can.execute();
					break;
			case "return":
				    while(stack.pop().getValue()!="returnFunct");
				    done=true;
				    break;
			case "efuncdef":
				while(stack.pop().getValue()!="returnFunct");
			    done=true;
			    break;
			case "push":
					stack.push(new Term(op,op));break;
			 default: 

						
			}
		}
		scope--;
	}

	private void saveVaiable(String op, Term term) {
		if(op.startsWith("globalvar_")){
			globalmap.put(op, term);
		}else{
			map.put(op, term);
		}
		
	}
	private Term getVaiable(String op) {
		if(op.startsWith("globalvar_")){
			return globalmap.get(op);
			}else{
			return  map.get(op);
		}
		
	}
}
