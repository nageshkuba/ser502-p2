package asu.msse.canyon.main;
import java.io.*;
import java.util.*;


public class Canyon {
	
	public static Map<String,MyFunction> func=new HashMap<String, MyFunction>();
	public int lineNo=0;
	public ExecutionManager mgr;
	
	/**
	 * @param args
	 */
	public Canyon(int lineNo){
		mgr=new ExecutionManager(lineNo);
		
	}
	public static void main(String[] args) {
		if (args.length<1){
			System.out.println("Input File name expected");
			System.exit(0);
		} 	
			
		else {
			try{
 			Canyon can = new Canyon(0);
			can.readFileInput(args);
			if (can.func.get("extras") != null) {
				MyFunction fun=can.func.get("extras");
				can.mgr.list = fun.instr;
				can.mgr.execute();
				
			}
			MyFunction fun=can.func.get("main");
			if(fun==null){
				System.out.println("Unable to locate main method.");
			}else
			{can = new Canyon(fun.startIndex);
			can.mgr.list = fun.instr;
			can.mgr.execute();
			}
		
		}catch(Exception e){
			if(e instanceof IOException){
				System.out.println("Invalid input file.");
			}else if(e instanceof ArithmeticException){
				System.out.println("Incompatible datatypes. Please verify the program.");
			}else {
				System.out.println("Unexpected operation encountered. Could not run the program.");
			}
		}}
	}


	private String getLinenumber(BufferedReader bf) throws IOException{
		String line  = bf.readLine();lineNo++;
		return line;
	}

	private void readFileInput(String[] args) throws IOException {
		String s = args[0];
		
		//System.out.println("File input:" + s);
		int i=1;
		try {
			File f = new File(s+".intr");
			FileReader rd = new FileReader(f);
			BufferedReader bf = new BufferedReader(rd);
			String line = "";

			while ((line=getLinenumber(bf)) != null) {
				String[] arr=line.split(" ");
				if(arr[0].trim().equals("funcdef")){
					MyFunction fun=new MyFunction(lineNo);
					func.put(arr[arr.length-1], fun);
					while ((line=getLinenumber(bf)) != null  && !line.trim().startsWith("efuncdef")) {
						fun.instr.add(line);
					}
				}else{
					MyFunction fun=func.get("extras");
					if(fun==null){
						fun=new MyFunction(lineNo);
						func.put("extras", fun);
					}
					fun.instr.add(line);
				}
				 
			}
		} catch (IOException e) {
			throw new IOException("Invalid file:"+s,e);
		}
	}

}
