#include "compiler.h"

Vector::Vector()
{
    elems = NULL;
    size = 0;
    cnt = 0;
}

void Vector::push_back (char* const& elem)
{
    if (size == 0)
    {
        int i = 0;
        size = 20;
        elems = new char*[sizeof(char*) * size];
        for (; i < size; i++)
        elems[i] = new char[256 * sizeof(char)];
    }

    if (size == cnt)
    {
        size *= 2;
        int i = size/2;
        elems = (char**)realloc(elems, sizeof(char*) * size);
        for (; i < size; i++)
        elems[i] = new char[256 * sizeof(char)];
    }
    strcpy(elems[cnt], elem);
    cnt++;
}

int Vector::count()
{
    return cnt;
}

char* Vector::pop(int index)
{
    if (index >= cnt)
    {
        return 0;
    }
    return elems[index];
}

void Vector::assign(int index, const char* str)
{
    if (index >= cnt)
    {
        return;
    }

    strcpy(elems[index], str);
}

void Vector::del()
{
    delete(elems);
}


Stack::Stack()
{
    elems = NULL;
    size = 0;
    cnt = 0;
}

void Stack::push (char* const& elem)
{ 
    if (size == 0)
    {
        size = 20;
        int i = 0;
        elems = new char*[sizeof(char*) * size];
        for (; i < size; i++)
        elems[i] = new char[256 * sizeof(char)];
    }

    if (size == cnt)
    {
        size *= 2;
        int i = size/2;
        elems = (char**)realloc(elems, sizeof(char*) * size);
        for (; i < size; i++)
        elems[i] = new char[256 * sizeof(char)];
    }

    strcpy(elems[cnt], elem);
    cnt++;
} 

int Stack::count() 
{ 
    return cnt;
} 

char* Stack::pop () 
{ 
    if (cnt> 0)
    {
        char* top = elems[cnt - 1];
        cnt--;
        return top;
    }

    return 0;
} 

char* Stack::top () const 
{ 
    if (cnt > 0)
    {
        return elems[cnt - 1];
    }

    return 0;
} 

void Stack::del()
{
    delete(elems);
}

