%{

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <map>
#include <string>
#include "compiler.h"

Stack backpatch;
Stack funcParams;
Stack breakstmt;
Stack funcargs;
Vector code_cell;

const char* currType;
char* incDec;
const char* funcName;
SymbolTable* activeTable;
SymbolTable* head;

int intr_count   = 0;
int local_count  = 0;
int global_count = 0;
int fdecl_count  = 0;
int errors       = 0;
int global_scope = 1;

const char* fetch_symbol_table(const char* identifier, int v);
void symbol_table_entry(const char* id, const char* type, bool is_var_symbol = true);
void typeSetting(const char* identifier, const char* type);
int intr_code_gen(const char*, ...); //variable arguments
char* stringVal(const char* format, ...); //variable arguments

extern FILE* yyin;
extern int yylineno;
extern int yylex();
extern int yyparse();
extern void yyerror(const char*);

%}

%error-verbose
%expect 1

%union {
    int i;
    double d;
    char* s;
}

%token<i> ADD SUB MUL DIV MOD INC DEC
%token<i> ADD_SUGAR SUB_SUGAR MUL_SUGAR DIV_SUGAR MOD_SUGAR
%token<i> EQ NE LT LTE GT OR GTE AND NOT COMMA SEMICOL EQUALS 
%token<i> OPEN_PARENTHESIS CLOSE_PARENTHESIS SCOPE_BEGIN SCOPE_END
%token<s> IF INT DBL STR FREF ELSE LOOP PRINT READ BREAK RETURN IDENTIFIER
%token<i> INT_LITERAL
%token<d> DBL_LITERAL
%token<s> STR_LITERAL

%%

program: code

code: lines | code lines;

lines: declarations | functions;

declarations:
    type_specifier init_declarator_list SEMICOL
    {
        currType = "";
    }
    ;

functions:
    type_specifier IDENTIFIER                
    {        
        global_scope = 0;
        funcName = $2;
        funcParams.push(stringVal("%s:", currType));

        symbol_table_entry(funcName, stringVal("%s:", currType), false);
        intr_code_gen("%s     %s", "funcdef", $2); 
        
    }
    OPEN_PARENTHESIS
    {
        SymbolTable* tbl = new SymbolTable();
        tbl->symboltable = head;
        activeTable= tbl;    
    }
    parameters CLOSE_PARENTHESIS
    {
        char* type_info = funcParams.pop();
        typeSetting(funcName, type_info);
    }
    block
    {        
        intr_code_gen("efuncdef");
        
        funcName = "";
        activeTable = head;
        global_scope = 1;
    }
    ;

func_declaration:
    OPEN_PARENTHESIS
    {
        backpatch.push(stringVal("%d", intr_count));
        intr_code_gen("_goto_fend");
        
        funcName = stringVal("%s_%d", "fdecl", fdecl_count++);
        funcParams.push("");
        symbol_table_entry(funcName, currType, false);
        
        intr_code_gen("%s     %s", "funcdef", funcName);
        
        SymbolTable* tbl = new SymbolTable();
        tbl->symboltable = activeTable;
        activeTable = tbl;    
        
    }
    block
    {
        intr_code_gen("efuncdef");
        char* str = backpatch.pop();
        code_cell.assign(atoi(str), stringVal("%s\t%d", "jmp", intr_count));    
        
        activeTable = activeTable->symboltable;
        funcName = "";
        currType = "";
    }
    ;
    
parameters:
    param
    | parameters COMMA param 
    ;

param:
    | INT IDENTIFIER
    {
        char* str = funcParams.pop();
        funcParams.push(stringVal("%s%s", str, "i"));
        symbol_table_entry($2, "i");
    }        
    | DBL IDENTIFIER
    {
        char* str = funcParams.pop();
        funcParams.push(stringVal("%s%s", str, "d"));
        symbol_table_entry($2, "d");
    }
    | STR IDENTIFIER
    {
        char* str = funcParams.pop();
        funcParams.push(stringVal("%s%s", str, "s"));
        symbol_table_entry($2, "s");
    }
    | FREF IDENTIFIER
    {
        char* str = funcParams.pop();
        funcParams.push(stringVal("%s%s", str, "f"));    
        symbol_table_entry($2, "f:");
    }
    ;

function:
    func_call_start arg_list CLOSE_PARENTHESIS
    {
        const char* type_info = fetch_symbol_table(funcName, 1);
        if (funcargs.count() != strlen(type_info) - 2 && strcmp(type_info, "f:")) {
            yyerror(stringVal("Function: '%s' expects %d argument(s), passed %d.", 
                    funcName, strlen(type_info) - 2,
                     funcargs.count()));
        }
        intr_code_gen("call");
        funcName = "";
        funcargs.del();
    }
    ;

func_call_start:
    IDENTIFIER OPEN_PARENTHESIS
    {    
        funcName = fetch_symbol_table($1, 0);
        if (!funcName)
            yyerror(stringVal("Unknown function: '%s', please define before usage.", funcName));
        intr_code_gen("%s       %s", "funcall", funcName);
    }
    ;
    
arg_list:
    | expr
    {
        funcargs.push("!");
        
    }
    | arg_list COMMA expr
    {
        funcargs.push("!");
    }
    ;
    
function_declaration:
    func_declaration OPEN_PARENTHESIS
    {
        funcName = stringVal("%s_%d", "anon", fdecl_count - 1);
        intr_code_gen("%s       %s", "funcall", funcName);
    }
    arg_list CLOSE_PARENTHESIS
    {
        const char* type_info = fetch_symbol_table(funcName, 1);
        if (funcargs.count() != strlen(type_info) - 2 && strcmp(type_info, "f:")) 
            yyerror(stringVal("Function: '%s' expects %d argument(s), passed %d.", 
                    funcName, strlen(type_info) - 2,
                     funcargs.count()));    
        
        intr_code_gen("call");
        funcName = "";
        funcargs.del();
    }
    ;
            
type_specifier:
    INT                
    { 
        currType = "i";
    }
    | DBL
    {
        currType = "d";
    }
    | STR
    {
        currType = "s";
    }
    | FREF
    {    
        currType = "f:";
    }
    ;

init_declarator_list:
    initialize
    | init_declarator_list COMMA initialize
    ;

initialize:
    IDENTIFIER                        
    { 
            //mostly for main function
            if (strcmp(currType, "f:"))
                intr_code_gen("%s        %s", "push", "funcId");
            symbol_table_entry($1, currType);
    }
    | IDENTIFIER EQUALS assignment_expr    
    {

        if (!strcmp(currType, "f:"))
        {
            const char* pch = strchr(fetch_symbol_table($<s>3, 1), ':');
            if (pch == NULL)
                yyerror(stringVal("'%s' cannot be assigned to function '%s'.",
                $<s>3, $1));
            else 
            {
                symbol_table_entry($1, fetch_symbol_table($<s>3, 1));            
            }
        }
        else
        {
            symbol_table_entry($1, currType);
        }
    }
    | IDENTIFIER EQUALS func_declaration
    {

        char* buffer = stringVal("%s_%d", "anon", fdecl_count - 1);
        intr_code_gen("%s       %s", "pushf", buffer);
        symbol_table_entry($1, fetch_symbol_table(buffer, 1));    

    }
    ;

block_local:
    block_local_code
    | block_local block_local_code
    ;

block_local_code:
    expression
    | declarations
    | block
    | if_block
    | loop_block
    | loop_exit
    | display_code
    | read_stmt
    | functions
    ;

expression:
    expr SEMICOL
    ;

block:
    SCOPE_BEGIN SCOPE_END
    | SCOPE_BEGIN
    {
        SymbolTable* tbl = new SymbolTable();
        tbl->symboltable = activeTable;
        activeTable = tbl;
    } 
    block_local SCOPE_END
    {
        activeTable = activeTable->symboltable;
    }
    ;

if_block:
    IF OPEN_PARENTHESIS expr CLOSE_PARENTHESIS
    {
        backpatch.push(stringVal("%d", intr_count));
        intr_code_gen("goto");
    }
    block_local_code
    {    
        char* str = backpatch.pop();
        code_cell.assign(atoi(str), stringVal("%s            %d", "goto", intr_count + 1));
        backpatch.push(stringVal("%d", intr_count));
        intr_code_gen("nop");
    }
    else_block
    ;
    
else_block:
    | ELSE block_local_code
    {
        char* str = backpatch.pop();
        code_cell.assign(atoi(str), stringVal("%s\t%d", "jmp", intr_count));
    }
    ;

loop:
    LOOP OPEN_PARENTHESIS
    {
        char* buffer = stringVal("%d", intr_count);
        backpatch.push(buffer);
        intr_code_gen("loop");
    }
    expr CLOSE_PARENTHESIS
    {
        backpatch.push(stringVal("%d", intr_count));
        intr_code_gen("goto");                                
    }
    block_local_code
    {
        char* str = backpatch.pop();
        code_cell.assign(atoi(str), stringVal("%s            %d", "goto", intr_count + 3));
        str = backpatch.pop();
        intr_code_gen("%s        %s", "goto", str);
        while (breakstmt.count() > 0) {
            str = breakstmt.pop();
            code_cell.assign(atoi(str), stringVal("%s\t%d", "jmp", intr_count));
        }
        intr_code_gen("eloop");
    }
    ;

display_code:
    PRINT OPEN_PARENTHESIS expr CLOSE_PARENTHESIS SEMICOL
    {
        intr_code_gen("disp");
    }
    ;

read_stmt:
    READ OPEN_PARENTHESIS IDENTIFIER CLOSE_PARENTHESIS SEMICOL
    {
        intr_code_gen("%s        %s", "read", fetch_symbol_table($3, 0));
    }
    ;

loop_block:
    loop
    ;

loop_exit:
    BREAK SEMICOL
    {
        breakstmt.push(stringVal("%d", intr_count));
        intr_code_gen("_break_stmt_");         
    }
    | RETURN expr SEMICOL
    {
        intr_code_gen("return");
    }
    ;

expr:
    assignment_expr
    ;

assignment_expr:
    or
    | IDENTIFIER EQUALS func_declaration
    {
        const char* pch = strchr(fetch_symbol_table($1, 1), ':');
        if (pch != NULL) {
            char* buffer = stringVal("%s_%d", "anon", fdecl_count - 1);
            typeSetting($1, fetch_symbol_table(buffer, 1));
            intr_code_gen("%s       %s", "pushf", buffer);
            intr_code_gen("%s       %s", "assnf", $1);
        }
        else {
            yyerror(stringVal("Cannot assign function to identifier '%s'.",
            $1));
        }
    }
    | IDENTIFIER EQUALS assignment_expr                
    {
        const char* pch = strchr(fetch_symbol_table($1, 1), ':');
        if (pch != NULL) {
            pch = strchr(fetch_symbol_table($<s>3, 1), ':');
            if (pch == NULL) {
                yyerror(stringVal("'%s' cannot be assigned to function '%s'.",
                $<s>3, $1));
            }
            typeSetting($1, fetch_symbol_table($<s>3, 1));
            intr_code_gen("%s       %s", "assnf", $1);
        }
        else
            intr_code_gen("%s        %s", "assn", fetch_symbol_table($1, 0));
    }
    | IDENTIFIER ADD_SUGAR assignment_expr            
    {
        intr_code_gen("%s       %s", "adda", fetch_symbol_table($1, 0));
    }
    | IDENTIFIER SUB_SUGAR assignment_expr            
    {
        intr_code_gen("%s       %s", "suba", fetch_symbol_table($1, 0));
    }
    | IDENTIFIER MUL_SUGAR assignment_expr            
    {
        intr_code_gen("%s       %s", "mula", fetch_symbol_table($1, 0));
    }
    | IDENTIFIER DIV_SUGAR assignment_expr            
    {
        intr_code_gen("%s       %s", "diva", fetch_symbol_table($1, 0));
    }
    | IDENTIFIER MOD_SUGAR assignment_expr            
    {
        intr_code_gen("%s       %s", "moda", fetch_symbol_table($1, 0));
    }
    ;

or:
    and
    | or OR and
    {
        intr_code_gen("or");
    }
    ;

and:
    equal
    | and AND equal
    {
        intr_code_gen("and");
    }
    ;

equal:
    relational
    | equal EQ relational
    {
        intr_code_gen("eq");
    }
    | equal NE relational
    {
        intr_code_gen("neq");
    }
    ;

relational:
    additive
    | relational LT additive
    {
        intr_code_gen("lt");
    }
    | relational LTE additive
    {
        intr_code_gen("lte");
    }
    | relational GT additive
    {
        intr_code_gen("gt");
    }
    | relational GTE additive
    {
        intr_code_gen("gte");
    }
    ;

additive:    
    multiplicative
    | additive ADD multiplicative
    {
        intr_code_gen("add");
    }
    | additive SUB multiplicative
    {
        intr_code_gen("sub");
    }            
    ;

multiplicative:    
    multiplicative MUL unary
    {
        intr_code_gen("mul");
    }
    | multiplicative DIV unary
    {
        intr_code_gen("div");
    }
    | multiplicative MOD unary
    {
        intr_code_gen("mod");
    }
    | unary
    ;

unary:
    postfix
    | NOT unary
    {
        intr_code_gen("not");
    }
    | ADD unary
    {
    }
    | SUB unary
    {
        intr_code_gen("neg");
    }
    | INC unary
    {
        intr_code_gen("++inc");
    }
    | DEC unary
    {
        intr_code_gen("--dec");
    }
    | function
    | function_declaration
    ;
    
postfix:
    prefix
    | postfix INC        
    {
        intr_code_gen("inc++");
        intr_code_gen("assn       %s", incDec);
    }
    | postfix DEC        
    {
        intr_code_gen("dec--"); 
        intr_code_gen("assn       %s", incDec);
    }
    ;

prefix:
    literal    
    | IDENTIFIER            
    {    
        const char* type_info = fetch_symbol_table($1, 1);
        const char* ptr = strchr(type_info, ':');
        if (ptr == NULL)
        {
            incDec = (char*)malloc(strlen(fetch_symbol_table($1, 0))*sizeof(char));
            strcpy(incDec,fetch_symbol_table($1, 0));
            intr_code_gen("%s    %s", "pushiden", fetch_symbol_table($1, 0));
        }else
            intr_code_gen("%s       %s", "pushf", fetch_symbol_table($1, 0));
    }
    | OPEN_PARENTHESIS expr CLOSE_PARENTHESIS
    | error
    ;

literal:
    INT_LITERAL        
    {
        intr_code_gen("%s     %d", "pushint", $1);
    }
    | DBL_LITERAL    
    {
        intr_code_gen("%s     %f", "pushdbl", $1);
    }
    | STR_LITERAL
    {
        intr_code_gen("%s     %s", "pushstr", $1);
    }
    ;

%%

void symbol_table_entry(const char* id, const char* type, bool ecode)
{
    string s(id);
    map<string, SymbolCell>& m = activeTable->table;
 
    char buffer[128];
 
    if (m.find(s) != m.end())
        yyerror(stringVal("'%s' is defined again and/or again.", id));
    else 
    {
        const char* ptr = strchr(type, ':');
        if (ptr != NULL)
            sprintf(buffer, "%s", id);
        else if (!global_scope)
            sprintf(buffer, "%s%d", "localvar_", local_count++);
        else
            sprintf(buffer, "%s%d", "globalvar_", global_count++);
 
        SymbolCell sv;
        sv.name = std::string(buffer);
        sv.type = type;    
        m[s] = sv;
    }

    if (ecode) {
        if (!strcmp(type, "i"))
            intr_code_gen("%s     %s", "saveint", buffer); 
        else if(!strcmp(type, "d"))
            intr_code_gen("%s     %s", "savedbl", buffer);
        else if(!strcmp(type, "s"))
            intr_code_gen("%s     %s", "savestr", buffer);
        else if(strchr(type, ':') != NULL)
            intr_code_gen("%s       %s", "savef", buffer);
    }
}

void typeSetting(const char* identifier, const char* type)
{    
    string str(identifier);
    map<string, SymbolCell> mtab;
    SymbolTable* symtab = activeTable;

    while(symtab->symboltable != NULL)
    {
        mtab = symtab->table;
        if (mtab.find(str) != mtab.end())
        {
            mtab[str].type = type;
            symtab->table = mtab;
            return;
        }
        else 
            symtab = symtab->symboltable;
    }

    if (symtab != NULL)
    {
        mtab = symtab ->table;
        if (mtab.find(str) != mtab.end())
        {
            mtab[str].type = type;
            symtab->table = mtab;
            return;
        }
    }

    yyerror(stringVal("Please declare '%s' before using.", identifier));        
}

char* stringVal(const char* format, ...)
{
    va_list ap;
    va_start(ap, format);

    char* buffer = (char*) malloc(128);
    vsprintf(buffer, format, ap);    
    va_end(ap);

    return buffer;
}

const char* fetch_symbol_table(const char* identifier, int v)
{
    string str(identifier);
    map<string, SymbolCell> mtab;
    SymbolTable* symtab = activeTable;

    while (symtab->symboltable != NULL) {
        mtab = symtab->table;

        if (mtab.find(str) != mtab.end())
        {
            if(0 == v)
                return mtab[str].name.c_str();
            else
                return mtab[str].type.c_str();
        }
        else 
            symtab = symtab->symboltable;
    }

    if (symtab != NULL) {
        mtab = symtab->table;
        if (mtab.find(str) != mtab.end())
        {
            if(0 == v)
                return mtab[str].name.c_str();
            else
                return mtab[string(identifier)].type.c_str();
        }
    }

    yyerror(stringVal("Please declare '%s' before using.", identifier));
    return "";
}

int intr_code_gen(const char* format, ...)
{
    va_list ap;
    va_start(ap, format);

    char buffer[128];
    int n = vsprintf(buffer, format, ap);    
    va_end(ap);
    code_cell.push_back(buffer);

    intr_count++;    
    return n;
}

void yyerror(const char* s)
{
    fprintf(stderr, "line %d: %s\n", yylineno, s);
    yyclearin;
    ++errors;
}

void cleanup()
{
    code_cell.del();
    backpatch.del();
    breakstmt.del();
    funcParams.del();
    funcargs.del();
}

int main(int argc, char** argv)
{
    if (argc < 2) 
    {
        fprintf(stderr, "Yikes! You forgot to provide an input file\n");
        return 1;
    }
    char* input = argv[1];
    char* fileExt = strstr(input, ".can");
//    const char* intrFile = "out.intr";
    
    char* name = (char*)malloc(strlen(input)*sizeof(char));
    strcpy(name, input);
    char *token = strtok(name, ".");
    char* filename = token;
    token = strtok(NULL, ".");
    strcat(filename, ".intr");
    char* intrFile = (char*)malloc(strlen(filename)*sizeof(char));    
    strcpy(intrFile, filename);
    if (!fileExt) 
    {
        fprintf(stderr, "Yikes! Please don't give me garbage. I cannot understand this extension, .can extension expected of '%s' file\n", argv[1]);
        return 1;
    }
    yyin = fopen(input, "r");
    if (yyin == NULL)
    {
        fprintf(stderr, "Yikes! Quit fooling, file '%s' does not exist\n", input);
        return 1;
    }
     
    FILE* fp = fopen(intrFile, "w+");
    head = new SymbolTable();
    head->symboltable = NULL;
    activeTable = head;

    printf("Compiling:       %s...\n", argv[1]);
    yyparse();
    printf("Errors:          %d\n", errors);
 
    if (errors) 
    {
        printf("Please fix the errors and try again\n");
        cleanup();
        exit(1);
    }
 
    for (int i = 0; i < code_cell.count(); i++)
    {
        fprintf(fp, "%s\n", code_cell.pop(i));
    }
    fclose(fp);
    cleanup();

    printf("Intermediate code file is: %s\n", intrFile);

    return 0;
}
