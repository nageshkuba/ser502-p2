#ifndef __COMPILER_H__
#define __COMPILER_H__

#include <iostream>
#include <string>
#include <map>

using namespace std;

struct SymbolCell
{
    string name;
    string type;
};

struct SymbolTable
{
    map<string, SymbolCell> table;
    SymbolTable* symboltable;
};

class Vector
{
    private:
        char** elems;
        int cnt;
        int size;

    public:
        Vector();
        void push_back(char* const&);
        char* pop(int);
        void assign(int, const char*);
        void del();
        int count();
};

class Stack 
{
    private:
        char** elems;
        int cnt;
        int size;

    public:
        Stack();
        void push(char* const&);
        char* pop();
        char* top() const;
        void del();
        int count();
};

#endif
