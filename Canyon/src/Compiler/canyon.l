%{

#include <stdlib.h>
#include "canyon.tab.h"

%}

%%

num         return INT;
dnum        return DBL;
words       return STR;
fref        return FREF;
if          return IF;
else        return ELSE;
loop        return LOOP;
break       return BREAK;
return      return RETURN;
display     return PRINT;
read        return READ;


-?(0|[1-9][0-9]*)                 {                
                                    yylval.i = atoi(yytext);
                                    return INT_LITERAL;
                                }
-?(0|[1-9][0-9]*)(\.|\.[0-9]*)    {                
                                    yylval.d = atof(yytext);
                                    return DBL_LITERAL;
                                }
[a-zA-Z_][a-zA-Z0-9_]*            {
                                    yylval.s = strdup(yytext); 
                                    return IDENTIFIER;
                                }
\"[^\"\n]*\"                    {
                                    yylval.s = strdup(yytext);
                                    return STR_LITERAL;
                                }
"//".*                            {}
"\n"                            { yylineno++; }

"[-"        return SCOPE_BEGIN;
"-]"        return SCOPE_END;
"("         return OPEN_PARENTHESIS;
")"         return CLOSE_PARENTHESIS;
"!"         return NOT;
"-"         return SUB;
"+"         return ADD;
"*"         return MUL;
"/"         return DIV;
"%"         return MOD;
"++"        return INC;
"--"        return DEC;
"&&"        return AND;
"<="        return LTE;
">="        return GTE;
"||"        return OR;
"<"         return LT;
">"         return GT;
"=="        return EQ;
"!="        return NE;
"+="        return ADD_SUGAR;
"-="        return SUB_SUGAR;
"*="        return MUL_SUGAR;
"/="        return DIV_SUGAR;
"%="        return MOD_SUGAR;
";"         return SEMICOL;
"="         return EQUALS;
","         return COMMA;

[ \t\v\f\r]+
.            printf("lexical error.\n");

%%

int yywrap()
{
    return 1;
}

